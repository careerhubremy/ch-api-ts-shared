import logdna from '@logdna/logger';
const loggerKey = `38877c7a58d9d473a0836f4bfab5fc1a`;
const options = {
	app: 'CareerHub',
	env: "debug",
}
const logger = logdna.createLogger(loggerKey, options)
const loggerMiddleware = (req, res, next) => {
	let message = `[${new Date}]\t[${req.method}]\t-\t${req.originalUrl}\t${req.headers["user-agent"]}`;
	logger.log(message)
	console.log(message)
	next && next()
}
export default loggerMiddleware;