import dotenv from 'dotenv'

dotenv.config()

import logger from "./middleware/logger";


const module = {
	logger: logger
}
export default module;