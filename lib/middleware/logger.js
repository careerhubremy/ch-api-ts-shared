"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = __importDefault(require("@logdna/logger"));
var loggerKey = "38877c7a58d9d473a0836f4bfab5fc1a";
var options = {
    app: 'CareerHub',
    env: "debug",
};
var logger = logger_1.default.createLogger(loggerKey, options);
var loggerMiddleware = function (req, res, next) {
    var message = "[".concat(new Date, "]\t[").concat(req.method, "]\t-\t").concat(req.originalUrl, "\t").concat(req.headers["user-agent"]);
    logger.log(message);
    console.log(message);
    next && next();
};
exports.default = loggerMiddleware;
